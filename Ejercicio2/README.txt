>> Ejercicio 2 <<

Al momento de ser ejecutado el programa se deberá responder por pantalla cuantas son las palabras que desea ingresar luego se permitirá ingresar las palabras, si decidos ingresar 4 palabras entonces de deberan escribir las 4 palabras, finalizando el programa se mostrará por pantalla la cantidad de mayusculas y minusculas que contienen las 
palabras ingresadas.

*Es importante recalcar que solo se deben ingresar palabras sin espacios, el ingreso de frases (utilizando espacios) hará que el programa no funcione de manera correcta.

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

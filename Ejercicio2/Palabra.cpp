#include <iostream>
#include <stdlib.h>
#include "Palabra.h"
#include <ctype.h>
#include <string.h>
#include <string>

using namespace std;

Palabra::Palabra(int dimension){
	this->dimension = dimension;
	this->vector = new string[this->dimension];	
}

void Palabra::ingrese_palabra(){
	cout << "Ingrese " << this->dimension << "palabras" << endl;
	
	string ayuda; 
	
	/* ingresar palabras */ 
	for (int i = 0; i < dimension; i++){
		cout << "--> " << endl;
		getline(cin, ayuda);
		vector[i] = ayuda; 
	}	
}

void Palabra::calcular(){
	char x[50];
	string ayuda;
	int mayus=0;
	int minus=0;
	
	for (int i = 0; i < dimension; i++){
		ayuda = vector[i];
		strcpy(x,ayuda.c_str());
		int longitud = strlen(x); 
		/* contar mayusculas y minusculas */ 
		for (int j = 0; j < longitud; j++){
			if(isupper(x[j])){
				mayus++;
			}
			else if(islower(x[j])){
				minus++;
			}
		}

		cout << vector[i] << " " << "tiene: " << mayus << " " << "mayusculas" << endl;
		cout << vector[i] << " " << "tiene: " << minus << " " << "minusculas" << endl;

		mayus = 0;
		minus = 0;
	}	
}

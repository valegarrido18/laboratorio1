#include <iostream>
#include <stdlib.h>
#include "Palabra.h"


using namespace std;

int main(){
	int cantidad;

	cout << "¿Cuántas palabras desea ingresar?: " << endl;
	cin >> cantidad;
	cin.ignore();
	Palabra palabra = Palabra(cantidad); 
	palabra.ingrese_palabra();
	palabra.calcular();
	return 0;
	
}


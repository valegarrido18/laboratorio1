#include <iostream>
#include <stdlib.h>

using namespace std;

#ifndef PALABRA_H
#define PALABRA_H

class Palabra {
	private:
		int dimension;
		string *vector = NULL;
		
	public: 
		/* Constructor*/
		Palabra (int dimension);
		
		void ingrese_palabra();
		void calcular();
};
#endif

#include <iostream>

using namespace std;

#ifndef CLIENTE_H
#define CLIENTE_H

class Cliente {

	private:
		string nombre;
		string telefono;
		int saldo;
		bool moroso=false;

		
	public:
		/* constructor */ 
		Cliente();
		
		void agregar_clientes();

		string get_nombre();
		string get_telefono();
		int get_saldo();
		bool get_moroso();
	
};
#endif

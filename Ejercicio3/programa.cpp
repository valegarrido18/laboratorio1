#include <stdlib.h>
#include <time.h>
#include <iostream>
#include "Cliente.h"

using namespace std;

int main(){
	int clientes;
	
	cout << "Cantidad de clientes: " << endl;
	cin >> clientes;
	
	Cliente arreglo[clientes];
	
	/* agregar clientes */ 
	for (int i = 0; i < clientes; i++){
		arreglo[i].agregar_clientes();
	}
	
	for (int j = 0; j < clientes; j++){
		if (arreglo[j].get_moroso() == true){
			cout << " " << endl;
			cout << "Moroso:  " << arreglo[j].get_nombre() << endl;
		}
	}


	return 0;
	
	 

}

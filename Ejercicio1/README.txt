>> Ejercicio 1 <<

El programa consiste en que el usuario debe ingresar por parámetros un valor el cual asignará la dimension que tendrá el arreglo, luego de acuerdo a esa dimension, se lanzaran numeros random por ejemplo, si ingreso el numero 4 por parámetro deberan salir 4 números de manera aleatoria y luego se sumará el cuadrado de todos los numeros que se encuentran en el arreglo mostrando finalmente por pantalla el resultado de esta operación. 

    • REQUISITOS PREVIOS
     
Sistema operativo Linux 
GNU Make (para compilar) 


-- EJECUTANDO LAS PRUEBAS POR TERMINAL --
Para compilar el código se debe agregar el comando make por terminal, luego de realizar la compilación, se utiliza el comando ./programa agregando un parámetro que dará la dimensión del arreglo. 

    • CONSTRUIDO CON:
      
Ubuntu: sistema operativo
Vim y Geany: editor de texto para escribir el código del programa.


    • VERSIONES
      
Ubuntu 18.04.1 LTS
GNU Make 4.1 

    • AUTORES

Valentina Garrido - Desarrollo del código, ejecución de proyecto y narración de README. 

    • EXPRESIONES DE GRATITUD

Ejemplos realizados en clases por Alejandro Valdés.

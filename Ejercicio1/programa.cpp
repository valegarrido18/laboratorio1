#include <iostream>
#include <stdlib.h>
#include "Sumacuadrado.h"

using namespace std;


int main(int argc, char *argv[]) {
	srand(time(NULL));
	
	int dimension = stoi(argv[1]);
	
	Sumacuadrado sumacuadrado = Sumacuadrado(dimension);
	sumacuadrado.procedimientos();
	
	return 0;
}

